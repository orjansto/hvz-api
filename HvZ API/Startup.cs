using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using HvZ_API.Entities;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using HvZ_API.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using HvZ_API.Services.IServices;
using HvZ_API.Services;

namespace HvZ_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddDbContext<HvZContext>(opt =>
               opt.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(AuthO =>
            {
                AuthO.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                AuthO.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(JwtOpt =>
            {
                JwtOpt.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
                        var userId = long.Parse(context.Principal.Identity.Name);
                        var user = userService.GetById(userId);
                        if (user == null)
                        {
                            context.Fail("Unauthorized");
                        }
                        //Console.Write(Task.CompletedTask);
                        return Task.CompletedTask;
                    }
                };
                JwtOpt.RequireHttpsMetadata = false;
                JwtOpt.SaveToken = true;
                JwtOpt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                };
            });
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAsyncService<CheckInEntity>, CheckInServiceAsync>();
            services.AddScoped<IAsyncService<KillEntity>, KillServiceAsync>();
            services.AddScoped<IAsyncService<MessageEntity>, MessageServiceAsync>();
            services.AddScoped<IAsyncService<GameEntity>, GameServiceAsync>();
            services.AddScoped<IAsyncService<MissionEntity>, MissionServiceAsync>();
            services.AddScoped<IAsyncService<PlayerEntity>, PlayerServiceAsync>();
            services.AddScoped<IAsyncJointService<SquadMemberEntity>, SquadMemeberServiceAsync>();
            services.AddScoped<IAsyncService<SquadEntity>, SquadServiceAsync>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
