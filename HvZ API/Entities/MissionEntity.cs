﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class MissionEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        [Required]
        [StringLength(49, MinimumLength = 3,
            ErrorMessage = "Game Title should be minimum 3 characters and maximum 49 characters long.")]
        public string Title { get; set; }

        public string Description { get; set; }
        [Required]
        public bool Human { get; set; }
        [Required]
        public bool Zombie { get; set; }

        public bool? IsSuccessful { get; set; } 
            //true: Mission is a success; 
            //false: Mission failed; 
            //NULL: Mission ongoing or not started

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }



    }
}
