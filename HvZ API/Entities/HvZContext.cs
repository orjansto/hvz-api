﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Entities
{
    public class HvZContext : DbContext
    {
        public HvZContext()
        {
        }
        public HvZContext(DbContextOptions<HvZContext> options)
            : base(options) 
        {
        }

        public DbSet<UserEntity> UserEntities { get; set; }
        public DbSet<AdminEntity> AdminEntities { get; set; }
        public DbSet<CheckInEntity> CheckInEntities { get; set; }
        public DbSet<GameEntity> GameEntities { get; set; }
        public DbSet<KillEntity> KillEntities { get; set; }
        public DbSet<MessageEntity> MessageEntities { get; set; }
        public DbSet<MissionEntity> MissionEntities { get; set; }
        public DbSet<PlayerEntity> PlayerEntities { get; set; }
        public DbSet<SquadEntity> SquadEntities { get; set; }
        public DbSet<SquadMemberEntity> SquadMemberEntities { get; set; }

    }
}
