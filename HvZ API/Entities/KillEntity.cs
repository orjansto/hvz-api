﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class KillEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        [Required]
        [ForeignKey("KillerId")]
        public int KillerId { get; set; }
        [Required]
        [ForeignKey("VictimId")]
        public int VictimId { get; set; }

        public DateTime TimeOfDeath { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
