﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class CheckInEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [ForeignKey("SquadId")]
        public int SquadId { get; set; }
        [Required]
        [ForeignKey("SquadMemberId")]
        public int SquadMemeberId { get; set; }
        [Required]
        public DateTime TimeCreated { get; set; }
        [Required]
        public double Longitude { get; set; }
        [Required]
        public double Latitude { get; set; }



    }
}
