﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class PlayerEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [ForeignKey("UserId")]
        public int UserId { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }

        [Required]
        public bool IsAlive { get; set; }
        [Required]
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }

    }
}
