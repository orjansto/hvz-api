﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HvZ_API.Entities
{
    public class GameEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(99, MinimumLength = 3,
            ErrorMessage = "Game Title should be minimum 3 characters and maximum 99 characters long.")]
        public string Title { get; set; }

        [Required]
        [RegularExpression("^registration$|^in progress$|^complete$", ErrorMessage ="Game state must be 'registration', 'in progress' or 'complete'")]
        public string State { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }



    }
}
