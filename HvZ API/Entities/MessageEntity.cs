﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class MessageEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        [Required]
        [ForeignKey("PlayerId")]
        public int PlayerId { get; set; }
        [ForeignKey("SquadId")]
        public int? SquadId { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 1,
            ErrorMessage = "Message can not exceed 200 characters.")]
        public string Text { get; set; }

        public DateTime TimeCreated { get; set; }


        public bool? IsHuman { get; set; } //True: Human; False: Zombie; NULL: Global
    }
}
