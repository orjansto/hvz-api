﻿using System.ComponentModel.DataAnnotations;

namespace HvZ_API.Entities
{
    public class UserEntity
    {
        [Key]
        public int Id { get; set; }

        [Required (ErrorMessage ="User {0} is required")]
        [StringLength (20,MinimumLength =3,
            ErrorMessage ="Username should be minimum 3 characters and maximum 20characters")]
        public string Username { get; set; }
        public byte[] PasswordSalt { get; set; }
        public byte[] PasswordHash { get; set; }

    }
}
