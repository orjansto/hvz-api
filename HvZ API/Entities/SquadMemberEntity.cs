﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class SquadMemberEntity
    {
        public int Id { get; set; }
        [ForeignKey("PlayerId")]
        public int PlayerId { get; set; }
        [ForeignKey("SquadId")]
        public int SquadId { get; set; }
    }
}
