﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HvZ_API.Entities
{
    public class SquadEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        [Required]
        public string SquadName { get; set; }
        [Required]
        [ForeignKey("SquadLeaderId")]
        public int SquadLeaderId { get; set; }
    }
}
