﻿using HvZ_API.Entities;
using HvZ_API.Helpers;
using HvZ_API.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace HvZ_API.Services
{
    public class MessageServiceAsync : IAsyncService<MessageEntity>


    {
        private HvZContext _context;
        public MessageServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<MessageEntity> Delete(int id)
        {
            var messageEntity = await _context.MessageEntities.FindAsync(id);
            if (messageEntity == null)
            {
                throw new AppException("message not found");
            }

            _context.MessageEntities.Remove(messageEntity);
            await _context.SaveChangesAsync();

            return messageEntity;
        }

        public async Task<IEnumerable<MessageEntity>> GetAll()
        {
            return await _context.MessageEntities.ToListAsync();
        }

        public async Task<MessageEntity> GetById(int id)
        {
            var messageEntity = await _context.MessageEntities.FindAsync(id);

            if (messageEntity == null)
            {
                throw new AppException("Message not found");
            }

            return messageEntity;
        }
       public async Task<MessageEntity> Create(MessageEntity messageEntity)
        {
            _context.MessageEntities.Add(messageEntity);
            await _context.SaveChangesAsync();

            return messageEntity;
        }


        public async Task<MessageEntity> Update(MessageEntity messageEntity)
        {
            _context.Entry(messageEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MessageEntityExists(messageEntity.Id))
                {
                    throw new AppException("message not found");
                }
                else
                {
                    throw;
                }
            }

            return messageEntity;
        }
        private bool MessageEntityExists(int id)
        {
            return _context.MessageEntities.Any(e => e.Id == id);
        }

        public Task<IEnumerable<MessageEntity>> GetAllById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}




