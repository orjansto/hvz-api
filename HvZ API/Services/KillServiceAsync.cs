﻿using HvZ_API.Entities;
using HvZ_API.Helpers;
using HvZ_API.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services
{
    public class KillServiceAsync : IAsyncService<KillEntity>
    {
        private HvZContext _context;
        public KillServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<KillEntity> Delete(int id)
        {
            var killEntity = await _context.KillEntities.FindAsync(id);
            if (killEntity==null)
            {
                throw new AppException("not found");
            }
            _context.KillEntities.Remove(killEntity);
            await _context.SaveChangesAsync();
            return (killEntity);
        }

        public async Task<IEnumerable<KillEntity>> GetAll()
        {

            return await _context.KillEntities.ToListAsync();
        }

        public async Task<KillEntity> GetById(int id)
        {
            var killEntity = await _context.KillEntities.FindAsync(id);
            if (killEntity==null)
            {
                throw new AppException("Not found");
            }
            return killEntity;
        }

        public async Task<KillEntity> Create(KillEntity killEntity)
        {
            _context.KillEntities.Add(killEntity);
            await _context.SaveChangesAsync();
            return (killEntity);

        }

        public async Task<KillEntity> Update(KillEntity killEntity)
        {
            _context.Entry(killEntity).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KillEntityExists(killEntity.Id))
                {
                    throw new AppException("not found");
                }
                else
                {
                    throw;
                }
            }

            return killEntity;
        }
        private bool KillEntityExists(int id)
        {
            return _context.KillEntities.Any(e => e.Id == id);
        }

        public Task<IEnumerable<KillEntity>> GetAllById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
