﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HvZ_API.Services.IServices
{
    public interface IAsyncService<T>
    {
        //GetAll
        Task<IEnumerable<T>> GetAll();
        //GetAllByID
        Task<IEnumerable<T>> GetAllById(int id);
        //GetByID
        Task<T> GetById(int id);
        //Update
        Task<T> Update(T obj);
        //CREATE
        Task<T> Create(T obj);
        //DELTE
        Task<T> Delete(int id);
    }
}
