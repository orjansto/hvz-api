﻿using HvZ_API.Entities;
using System.Collections.Generic;

namespace HvZ_API.Services.IServices
{
    public interface IUserService
    {
        UserEntity Authenticate(string username, string password);
        IEnumerable<UserEntity> GetAll();
        UserEntity GetById(long id);
        UserEntity Create(UserEntity userInfo, string password);
        void Update(UserEntity user, string password = null);
        void Delete(long id);
    }
}
