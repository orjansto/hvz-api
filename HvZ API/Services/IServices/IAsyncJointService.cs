﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HvZ_API.Services.IServices
{
    public interface IAsyncJointService<T>
    {
        Task<IEnumerable<T>> GetAllEntitiesByPlayerId(int id);
        Task<IEnumerable<T>> GetAllEntitiesBySquadId(int id);
        Task<T> GetById(int id);
        Task<T> Create(T obj);
        Task<T> Delete(int id);
    }
}
