﻿using HvZ_API.Entities;
using HvZ_API.Helpers;
using HvZ_API.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HvZ_API.Services
{
    public class UserService : IUserService
    {
        private HvZContext _hvZContext;

        public UserService(HvZContext hvZContext)
        {
            _hvZContext = hvZContext;
        }

        //Log-in
        public UserEntity Authenticate(string username, string password)
        {
            if(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = _hvZContext.UserEntities.FirstOrDefault(user => user.Username == username);

            if(user == null)
            {
                return null;
            }

            if(!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }
            return user;
        }

        //Register
        public UserEntity Create(UserEntity userInfo, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new AppException("Password is required");
            }

            if (_hvZContext.UserEntities.Any(user => user.Username == userInfo.Username))
            {
                throw new AppException("Username \"" + userInfo.Username + "\" is already taken");
            }

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            userInfo.PasswordHash = passwordHash;
            userInfo.PasswordSalt = passwordSalt;

            _hvZContext.UserEntities.Add(userInfo);
            _hvZContext.SaveChanges();

            return userInfo;
        }

        public void Delete(long id)
        {
            var user = _hvZContext.UserEntities.Find(id);

            if(user != null)
            {
                _hvZContext.UserEntities.Remove(user);
                _hvZContext.SaveChanges();
            }
            else
            {
                throw new AppException("No user with id: "+id+" found in the db. Nothing deleted");
            }
        }

        public void Update(UserEntity user, string password = null)
        {
            var userInfo = _hvZContext.UserEntities.Find(user.Id);

            if(userInfo == null)
            {
                throw new AppException("User not found");
            }

            if(user.Username != userInfo.Username)
            {
                if(_hvZContext.UserEntities.Any(x => x.Username == user.Username))
                {
                    throw new AppException("The username " + user.Username + " is already taken");
                }
            }

            userInfo.Username = user.Username;

            if(!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                userInfo.PasswordHash = passwordHash;
                userInfo.PasswordSalt = passwordSalt;
            }

            _hvZContext.UserEntities.Update(userInfo);
            _hvZContext.SaveChanges();
        }

        public IEnumerable<UserEntity> GetAll()
        {
            return _hvZContext.UserEntities;
        }

        public UserEntity GetById(long id)
        {

            return _hvZContext.UserEntities.Find((int)id);
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            if (storedHash.Length != 64)
            {
                throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            }

            if (storedSalt.Length != 128)
            {
                throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");
            }

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
