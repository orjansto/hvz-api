﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvZ_API.Services.IServices;
using HvZ_API.Entities;
using HvZ_API.Helpers;

namespace HvZ_API.Services
{
    public class AdminServiceAsync : IAsyncJointService<AdminEntity>
    {

        private HvZContext _context;
        public AdminServiceAsync (HvZContext context)
        {
            _context = context;
        }
        public async Task<AdminEntity> Create(AdminEntity adminEntity)
        {
            _context.AdminEntities.Add(adminEntity);
            await _context.SaveChangesAsync();

            return adminEntity;
        }

        public async Task<AdminEntity> Delete(int id)
        {
            var adminEntity = await _context.AdminEntities.FindAsync(id);
            if (adminEntity == null)
            {
                throw new AppException("squadMember not found");
            }

            _context.AdminEntities.Remove(adminEntity);
            await _context.SaveChangesAsync();

            return adminEntity;
        }

        public Task<IEnumerable<AdminEntity>> GetAllEntitiesByPlayerId(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<AdminEntity>> GetAllEntitiesBySquadId(int id)
        {
            throw new NotImplementedException();
        }

        public Task<AdminEntity> GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
