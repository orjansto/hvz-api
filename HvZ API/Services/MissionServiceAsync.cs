﻿using HvZ_API.Entities;
using HvZ_API.Helpers;
using HvZ_API.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace HvZ_API.Services
{
    public class MissionServiceAsync : IAsyncService<MissionEntity>

    {
        private HvZContext _context;
        public MissionServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<MissionEntity> Delete(int id)
        {
            var missionEntity = await _context.MissionEntities.FindAsync(id);
            if (missionEntity == null)
            {
                throw new AppException("mission not found");
            }

            _context.MissionEntities.Remove(missionEntity);
            await _context.SaveChangesAsync();

            return missionEntity;
        }

        public async Task<IEnumerable<MissionEntity>> GetAll()
        {
            return await _context.MissionEntities.ToListAsync();
        }

        public async Task<MissionEntity> GetById(int id)
        {
            var missionEntity = await _context.MissionEntities.FindAsync(id);

            if (missionEntity == null)
            {
                throw new AppException("Mission not found");
            }

            return missionEntity;
        }

        public async Task<MissionEntity> Create(MissionEntity missionEntity)
        {
            _context.MissionEntities.Add(missionEntity);
            await _context.SaveChangesAsync();

            return missionEntity;
        }


        public async Task<MissionEntity> Update(MissionEntity missionEntity)
        {
            _context.Entry(missionEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MissionEntityExists(missionEntity.Id))
                {
                    throw new AppException("mission not found");
                }
                else
                {
                    throw;
                }
            }

            return missionEntity;
        }
        private bool MissionEntityExists(int id)
        {
            return _context.MissionEntities.Any(e => e.Id == id);
        }

        public Task<IEnumerable<MissionEntity>> GetAllById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}



