﻿using HvZ_API.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvZ_API.Entities;
using HvZ_API.Helpers;
using Microsoft.EntityFrameworkCore;

namespace HvZ_API.Services
{
    public class PlayerServiceAsync : IAsyncService<PlayerEntity>
    {
        private HvZContext _context;
        public PlayerServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<PlayerEntity> Delete(int id)
        {
            var playerEntity = await _context.PlayerEntities.FindAsync(id);
            if (playerEntity == null)
            {
                throw new AppException("player not found");
            }

            _context.PlayerEntities.Remove(playerEntity);
            await _context.SaveChangesAsync();

            return playerEntity;
        }

        public async Task<IEnumerable<PlayerEntity>> GetAll()
        {
            return await _context.PlayerEntities.ToListAsync();
        }

        public async Task<PlayerEntity> GetById(int id)
        {
            var playerEntity = await _context.PlayerEntities.FindAsync(id);

            if (playerEntity == null)
            {
                throw new AppException("player not found");
            }

            return playerEntity;
        }

        public async Task<PlayerEntity> Create(PlayerEntity playerEntity)
        {
            _context.PlayerEntities.Add(playerEntity);
            await _context.SaveChangesAsync();

            return playerEntity;
        }

        public async Task<PlayerEntity> Update(PlayerEntity playerEntity)
        {
            _context.Entry(playerEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerEntityExists(playerEntity.Id))
                {
                    throw new AppException("player not found");
                }
                else
                {
                    throw;
                }
            }

            return playerEntity;
        }
        private bool PlayerEntityExists(int id)
        {
            return _context.PlayerEntities.Any(e => e.Id == id);
        }

        public async Task<IEnumerable<PlayerEntity>> GetAllById(int id)
        {
           return await _context.PlayerEntities.Where(player => player.GameId == id).ToListAsync(); ;
        }
    }
}
