﻿using HvZ_API.Entities;
using HvZ_API.Helpers;
using HvZ_API.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services
{
    public class SquadServiceAsync : IAsyncService<SquadEntity>
    {
        private HvZContext _context;
        public SquadServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<SquadEntity> Delete(int id)
        {
            var squadEntity = await _context.SquadEntities.FindAsync(id);
            if (squadEntity == null)
            {
                throw new AppException("Squad not found");
            }

            _context.SquadEntities.Remove(squadEntity);
            await _context.SaveChangesAsync();

            return squadEntity;
        }

        public async Task<IEnumerable<SquadEntity>> GetAll()
        {
            return await _context.SquadEntities.ToListAsync();
        }

        public async Task<SquadEntity> GetById(int id)
        {
            var squadEntity = await _context.SquadEntities.FindAsync(id);

            if (squadEntity == null)
            {
                throw new AppException("Squad not found");
            }

            return squadEntity;
        }

        public async Task<SquadEntity> Create(SquadEntity squadEntity)
        {
            _context.SquadEntities.Add(squadEntity);
            await _context.SaveChangesAsync();

            return squadEntity;
        }

        public async Task<SquadEntity> Update(SquadEntity squadEntity)
        {
            _context.Entry(squadEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SquadEntityExists(squadEntity.Id))
                {
                    throw new AppException("Squad not found");
                }
                else
                {
                    throw;
                }
            }

            return squadEntity;
        }
        private bool SquadEntityExists(int id)
        {
            return _context.SquadEntities.Any(e => e.Id == id);
        }

        public Task<IEnumerable<SquadEntity>> GetAllById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
