﻿using HvZ_API.Entities;
using HvZ_API.Services.IServices;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvZ_API.Helpers;
using Microsoft.EntityFrameworkCore;


namespace HvZ_API.Services
{
    public class GameServiceAsync : IAsyncService<GameEntity>
    {
        private HvZContext _context;

        public GameServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<GameEntity> Delete(int id)
        {
            var gameEntity = await _context.GameEntities.FindAsync(id);
            if (gameEntity == null)
            {
                throw new AppException("Game not found");
            }

            _context.GameEntities.Remove(gameEntity);
            await _context.SaveChangesAsync();

            return gameEntity;
        }

        public async Task<IEnumerable<GameEntity>> GetAll()
        {
            return await _context.GameEntities.ToListAsync();

        }

        public async Task<GameEntity> GetById(int id)
        {
            var gameEntity = await _context.GameEntities.FindAsync(id);
            if (gameEntity==null)
            {
                throw new AppException("Game not found");
            }
            return gameEntity;

        }

        public async Task<GameEntity> Create(GameEntity gameEntity)
        {
            _context.GameEntities.Add(gameEntity);
            await _context.SaveChangesAsync();
            return gameEntity;
        }

        public async Task<GameEntity> Update(GameEntity gameEntity)
        {
            _context.Entry(gameEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameEntityExists(gameEntity.Id))
                {
                    throw new AppException("game not found");
                }
                else
                {
                    throw;
                }
            }

            return gameEntity;
        }
        private bool GameEntityExists(int id)
        {
            return _context.GameEntities.Any(e => e.Id == id);
        }

        public Task<IEnumerable<GameEntity>> GetAllById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}




