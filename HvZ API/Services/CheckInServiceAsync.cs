﻿using HvZ_API.Entities;
using HvZ_API.Helpers;
using HvZ_API.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services
{
    public class CheckInServiceAsync : IAsyncService<CheckInEntity>
    {
        private HvZContext _context;
        public CheckInServiceAsync(HvZContext context)
        {
            _context = context;
        }
        public async Task<CheckInEntity> Delete(int id)
        {
            var checkInEntity = await _context.CheckInEntities.FindAsync(id);
            if (checkInEntity == null)
            {
                throw new AppException("CheckIn not found");
            }
            _context.CheckInEntities.Remove(checkInEntity);
             await _context.SaveChangesAsync();
            return checkInEntity;
        }

        public async Task<IEnumerable<CheckInEntity>> GetAll()
        {
            return await _context.CheckInEntities.ToListAsync();

        }

        public async Task<CheckInEntity> GetById(int id)
        {
            var checkInEntity = await _context.CheckInEntities.FindAsync(id);
            if (checkInEntity == null)
            {
                throw new AppException("CheckIn not found");
            }
            return checkInEntity;
        }

        public async Task<CheckInEntity> Create(CheckInEntity checkInEntity)
        {
            _context.CheckInEntities.Add(checkInEntity);
            await _context.SaveChangesAsync();
            return checkInEntity;
        }

        public async Task<CheckInEntity> Update(CheckInEntity checkInEntity)
        {
            _context.Entry(checkInEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckInEntityExists(checkInEntity.Id))
                {
                    throw new AppException("CheckIn not found");
                }
                else
                {
                    throw;
                }
            }

            return checkInEntity;
        }
        private bool CheckInEntityExists(int id)
        {
            return _context.CheckInEntities.Any(e => e.Id == id);
        }

        public Task<IEnumerable<CheckInEntity>> GetAllById(int id)
        {
            throw new NotImplementedException();
        }
    }
}