﻿using HvZ_API.Services.IServices;
using HvZ_API.Entities;
using HvZ_API.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace HvZ_API.Services
{
    public class SquadMemeberServiceAsync : IAsyncJointService<SquadMemberEntity>
    {

        private HvZContext _context;
        public SquadMemeberServiceAsync(HvZContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SquadMemberEntity>> GetAllEntitiesByPlayerId(int id)
        {
            var squads = await _context.SquadMemberEntities.Where(player => player.PlayerId == id).ToListAsync();

            return squads;
        }
        public async Task<SquadMemberEntity> Create(SquadMemberEntity squadMemberEntity)
        {
            _context.SquadMemberEntities.Add(squadMemberEntity);
            await _context.SaveChangesAsync();

            return squadMemberEntity;
        }

        public async Task<SquadMemberEntity> Delete(int id)
        {
            var squadMemberEntity = await _context.SquadMemberEntities.FindAsync(id);
            if (squadMemberEntity == null)
            {
                throw new AppException("squadMember not found");
            }

            _context.SquadMemberEntities.Remove(squadMemberEntity);
            await _context.SaveChangesAsync();

            return squadMemberEntity;
        }

        public async Task<SquadMemberEntity> GetById(int id)
        {
            var squadMemberEntity = await _context.SquadMemberEntities.FindAsync(id);

            if (squadMemberEntity == null)
            {
                throw new AppException("squadMember not found");
            }

            return squadMemberEntity;
        }

        public async Task<IEnumerable<SquadMemberEntity>> GetAllEntitiesBySquadId(int id)
        {
            var squads = await _context.SquadMemberEntities.Where(squad => squad.Id == id).ToListAsync();

            return squads;
        }
    }
}
