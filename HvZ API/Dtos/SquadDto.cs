﻿using System.Collections.Generic;

namespace HvZ_API.Dtos
{
    public class SquadDto
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string SquadName { get; set; }
        public int SquadLeaderId { get; set; }
        public List<PlayerDto> SquadMember { get; set; }
    }
}
