﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Dtos
{
    public class PlayerKillSquadDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
        public bool IsAlive { get; set; }
        public bool IsPatienZero { get; set; }
        public string BiteCode { get; set; }
        public List<SquadDto> squads { get; set; }
        public List<KillDto> kills { get; set; }
    }
}
