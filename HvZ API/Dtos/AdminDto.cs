﻿namespace HvZ_API.Dtos
{
    public class AdminDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
    }
}
