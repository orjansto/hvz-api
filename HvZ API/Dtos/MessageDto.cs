﻿using System;

namespace HvZ_API.Dtos
{
    public class MessageDto
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int PlayerId { get; set; }
        public int? SquadId { get; set; }
        public string Text { get; set; }
        public DateTime TimeCreated { get; set; }
        public bool? IsHuman { get; set; } //True: Human; False: Zombie; NULL: Global
    }
}
