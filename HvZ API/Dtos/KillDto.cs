﻿using System;

namespace HvZ_API.Dtos
{
    public class KillDto
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int KillerId { get; set; }
        public int VictimId { get; set; }
        public DateTime TimeOfDeath { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
