﻿using System;

namespace HvZ_API.Dtos
{
    public class CheckInDto
    {
        public int Id { get; set; }
        public int SquadId { get; set; }
        public int SquadMemeberId { get; set; }
        public DateTime TimeCreated { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
