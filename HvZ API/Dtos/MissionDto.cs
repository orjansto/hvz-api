﻿using System;

namespace HvZ_API.Dtos
{
    public class MissionDto
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }        
        public bool Human { get; set; }
        public bool Zombie { get; set; }
        public bool IsActive { get; set; }
        public bool IsSuccessful { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
