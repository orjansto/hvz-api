﻿using System.Collections.Generic;

namespace HvZ_API.Dtos
{
    public class PlayerDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
        public bool IsAlive { get; set; }
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }
    }
}
