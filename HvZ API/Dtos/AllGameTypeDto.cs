﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Dtos
{
    public class AllGameTypeDto
    {
        public List<GameDto> Registered { get; set; }
        public List<GameDto> Games { get; set; }
        public List<GameDto> Finished { get; set; }
    }
}
