﻿using System;
using System.Collections.Generic;

namespace HvZ_API.Dtos
{
    public class GameDto
    {
        public int Id { get; set; }
        public string State { get; set; }
        public string Title { get; set; }
        public TimeSpan duration { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int NumberOfPlayers { get; set; }
        public bool registered { get; set; }
        public List<SquadDto> squads { get; set; }
    }
}
