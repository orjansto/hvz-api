﻿using AutoMapper;
using HvZ_API.Dtos;
using HvZ_API.Entities;

namespace HvZ_API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CheckInDto, CheckInEntity>();
            CreateMap<CheckInEntity, CheckInDto>();

            CreateMap<GameDto, GameEntity>();
            CreateMap<GameEntity, GameDto>();

            CreateMap<KillDto, KillEntity>();
            CreateMap<KillEntity, KillDto>();

            CreateMap<MessageDto, MessageEntity>();
            CreateMap<MessageEntity, MessageDto>();

            CreateMap<MissionDto, MissionEntity>();
            CreateMap<MissionEntity, MissionDto>();

            CreateMap<PlayerDto, PlayerEntity>();
            CreateMap<PlayerEntity, PlayerDto>();

            CreateMap<SquadDto, SquadEntity>();
            CreateMap<SquadEntity, SquadDto>();

            CreateMap<UserDto, UserEntity>();
            CreateMap<UserEntity, UserDto>();

            CreateMap<PlayerKillSquadDto, PlayerEntity>();
        }
    }
}
