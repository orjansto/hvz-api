﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using HvZ_API.Entities;
using Microsoft.AspNetCore.Authorization;
using HvZ_API.Services.IServices;
using AutoMapper;
using HvZ_API.Dtos;
using HvZ_API.Helpers;
using System.Threading.Tasks;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CheckInController : ControllerBase
    {
        private readonly IAsyncService<CheckInEntity> _service;
        private IMapper _mapper;

        public CheckInController(IAsyncService<CheckInEntity> service, IMapper mapper)
        {
             _service = service;
             _mapper = mapper;
        }

        // GET: api/CheckIn
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CheckInEntity>>> GetCheckInEntities()
        {
            var checkIns = await _service.GetAll();
            var checkInsDto = _mapper.Map<IList<CheckInDto>>(checkIns);
            return Ok(checkInsDto);
        }

        // GET: api/CheckIn/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CheckInEntity>> GetCheckInEntity(int id)
        {
            var checkIn =await _service.GetById(id);

            if(checkIn == null)
            {
                return NotFound();
            }

            var checkInDto = _mapper.Map<CheckInDto>(checkIn);
            return Ok(checkInDto);
        }
  
        // POST: api/CheckIn
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CheckInEntity>> PostCheckInEntity([FromBody] CheckInDto checkInDto)
        {
             var checkInEntity = _mapper.Map<CheckInEntity>(checkInDto);

            try
            {
               var checkIn =await _service.Create(checkInEntity);
                checkInDto = _mapper.Map<CheckInDto>(checkIn);
                return CreatedAtAction(nameof(GetCheckInEntity), new { id = checkInDto.Id }, checkInDto);
            }
            catch(AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/CheckIn/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CheckInEntity>> DeleteCheckInEntity(int id)
        {
            try
            {
                var checkIn = await _service.Delete(id);
                return Ok(checkIn);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

    }
}
