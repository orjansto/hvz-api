﻿using Microsoft.AspNetCore.Mvc;
using HvZ_API.Entities;
using HvZ_API.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using HvZ_API.Dtos;
using AutoMapper;
using HvZ_API.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/Game/{gameId}/player/{playerId}[controller]")]
    [ApiController]
    public class KillController : ControllerBase
    {
        private readonly IAsyncService<KillEntity> _service;
        private IMapper _mapper;

        public KillController(IAsyncService<KillEntity> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/Kill
        [HttpGet]
        public async Task<ActionResult<IEnumerable<KillEntity>>> GetKillEntities()
        {
            var kills = await _service.GetAll();
            var killsDto = _mapper.Map<IList<KillDto>>(kills);
            return Ok(killsDto);
        }

        // GET: api/Kill/5
        [HttpGet("{id}")]
        public async Task<ActionResult<KillEntity>> GetKillEntity(int id)
            
        {
            var kill = await _service.GetById(id);

            if (kill == null)
            {
                return NotFound();
            }

            var killDto = _mapper.Map<KillDto>(kill);

            return Ok(killDto);
        }

        // PUT: api/Kill/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut]
        public async Task<IActionResult> PutKillEntity(int id, [FromBody] KillDto killDto)
        {
            var killEntity = _mapper.Map<KillEntity>(killDto);
            killEntity.Id = id;
            try
            {
                var kill = await _service.Update(killEntity);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Kill
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<KillEntity>> PostKillEntity([FromBody] KillDto killDto)
        {
            var killEntity = _mapper.Map<KillEntity>(killDto);

            try
            {
                var kill = await _service.Create(killEntity);
                killDto = _mapper.Map<KillDto>(kill);

                return CreatedAtAction(nameof(GetKillEntity), new { id = killDto.Id }, killDto);
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }
        // DELETE: api/Kill/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<KillEntity>> DeleteKillEntity(int id)
        {
            try
            {
                var kill = await _service.Delete(id);
                return Ok(kill);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }
    }
}
