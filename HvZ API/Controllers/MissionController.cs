﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using HvZ_API.Entities;
using HvZ_API.Services.IServices;
using AutoMapper;
using HvZ_API.Dtos;
using HvZ_API.Helpers;
using System.Threading.Tasks;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MissionController : ControllerBase
    {
        private readonly IAsyncService<MissionEntity> _service;
        private IMapper _mapper;


        public MissionController(IAsyncService<MissionEntity> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/Mission
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MissionEntity>>> GetMissionEntities()
        {
            var missions = await _service.GetAll();
            var missionsDto = _mapper.Map<IList<MissionDto>>(missions);
            return Ok(missionsDto);
        }

        // GET: api/Mission/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MissionEntity>> GetMissionEntity(int id)
        {
            var mission = await _service.GetById(id);

            if (mission == null)
            {
                return NotFound();
            }
            var missionDto = _mapper.Map<MissionDto>(mission);

            return Ok(missionDto);
        }

        // PUT: api/Mission/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMissionEntity(int id, [FromBody] MissionDto missionDto)
        {
            var missionEntity = _mapper.Map<MissionEntity>(missionDto);
            missionEntity.Id = id;
            try
            {
                var mission = await _service.Update(missionEntity);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Mission
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MissionEntity>> PostMissionEntity([FromBody] MissionDto missionDto)
        {
            var missionEntity = _mapper.Map<MissionEntity>(missionDto);

            try
            {
                var mission = await _service.Create(missionEntity);
                missionDto = _mapper.Map<MissionDto>(mission);

                return CreatedAtAction(nameof(GetMissionEntity), new { id = missionDto.Id }, missionDto);
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Mission/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MissionEntity>> DeleteMissionEntity(int id)
        {
            try
            {
                var mission = await _service.Delete(id);
                return Ok(mission);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }


    }
}
