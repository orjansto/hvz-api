﻿using Microsoft.AspNetCore.Mvc;
using HvZ_API.Entities;
using HvZ_API.Services.IServices;
using AutoMapper;
using HvZ_API.Dtos;
using HvZ_API.Helpers;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/Game/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly IAsyncService<PlayerEntity> _playerService;
        private readonly IAsyncService<KillEntity> _killService;
        private readonly IAsyncService<SquadEntity> _squadService;
        private readonly IAsyncJointService<SquadMemberEntity> _squadMemberService;
        private IMapper _mapper;

        public PlayerController(
            IAsyncService<PlayerEntity> playerService,
            IAsyncService<KillEntity> killService,
            IAsyncService<SquadEntity> squadService,
            IAsyncJointService<SquadMemberEntity> squadMemberService,
            IMapper mapper)
        {
            _playerService = playerService;
            _killService = killService;
            _squadService = squadService;
            _squadMemberService = squadMemberService;
            _mapper = mapper;
        }

        // GET: api/Player
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerEntity>>> GetPlayerEntities()
        {
            var players = await _playerService.GetAll();
            var playersDto = _mapper.Map<IList<PlayerDto>>(players);
            return Ok(playersDto);
        }


        // GET: api/Player/5
        [HttpGet("{id}")]
        
        public async Task<ActionResult<PlayerEntity>> GetPlayerEntity(int id)
        {
            var player = await _playerService.GetById(id);

            if (player == null)
            {
                return NotFound();
            }
            var playerKillSquadDto = _mapper.Map<PlayerKillSquadDto>(player);

            var squads = await _squadService.GetAll();
            var kills = await _killService.GetAll();
            var squadMembers = await _squadMemberService.GetAllEntitiesByPlayerId(player.Id);

            foreach(SquadMemberEntity squadmember in squadMembers)
            {  
                foreach(SquadEntity squad in squads) {
                    if(squadmember.Id == squad.Id)
                    {
                        var squadDto = _mapper.Map<SquadDto>(squad);

                        var squadPlayers = await _playerService.GetAll();
                        foreach(PlayerEntity squadPlayer in squadPlayers)
                        {
                            if(squadPlayer.Id == squadmember.PlayerId)
                            {
                                squadDto.SquadMember.Add(_mapper.Map<PlayerDto>(squadPlayer));
                            }
                        }
                        playerKillSquadDto.squads.Add(squadDto);
                    }
                }
            }
            
            foreach(KillEntity kill in kills)
            {
                if(playerKillSquadDto.Id == kill.KillerId)
                {
                    playerKillSquadDto.kills.Add(_mapper.Map<KillDto>(kill));
                }
            }

            return Ok(playerKillSquadDto);
        }

        // PUT: api/Player/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayerEntity(int id, [FromBody] PlayerDto playerDto)
        {
            var playerEntity = _mapper.Map<PlayerEntity>(playerDto);
            playerEntity.Id = id;
            try
            {
                var player = await _playerService.Update(playerEntity);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Player
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<PlayerEntity>> PostPlayerEntity([FromBody] PlayerDto playerDto)
        {
            var playerEntity = _mapper.Map<PlayerEntity>(playerDto);

            playerEntity.UserId = int.Parse(User.Identity.Name);

            try
            {
                var player = await _playerService.Create(playerEntity);
                playerDto = _mapper.Map<PlayerDto>(player);

                return CreatedAtAction(nameof(GetPlayerEntity), new { id = playerDto.Id }, playerDto);
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Player/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PlayerEntity>> DeletePlayerEntity(int id)
        {
            try
            {
                var player = await _playerService.Delete(id);
                return Ok(player);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

    }
}
