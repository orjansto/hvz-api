﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HvZ_API.Entities;
using Microsoft.AspNetCore.Authorization;
using HvZ_API.Services.IServices;
using HvZ_API.Dtos;
using HvZ_API.Helpers;
using AutoMapper;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SquadController : ControllerBase
    {
        //private readonly HvZContext _context;
        private readonly IAsyncService<SquadEntity> _service;
        private readonly IAsyncJointService<SquadMemberEntity> _sqaudMemberService;
        private IMapper _mapper;

        public SquadController(
            IAsyncService<SquadEntity> service,
            IAsyncJointService<SquadMemberEntity> squadMemberService,
            IMapper mapper)
        {
            _service = service;
            _sqaudMemberService = squadMemberService;
            _mapper = mapper;
        }
        //private readonly AppSettings _appSettings;


        // GET: api/Squad
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SquadEntity>>> GetSquadEntities()
        {
            var squads = await _service.GetAll();
            var squadsDto = _mapper.Map<IList<SquadDto>>(squads);
            return Ok(squadsDto);
        }
        // GET: api/Squad/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SquadEntity>> GetSquadEntity(int id)
        {
            var squadEntity = await _service.GetById(id);

            if (squadEntity == null)
            {
                return NotFound();
            }
            var squadDto = _mapper.Map<SquadDto>(squadEntity);
            return Ok(squadDto);
        }
        // PUT: api/Squad/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSquadEntity(int id, [FromBody] SquadDto squadDto)
        {
            //if(id != squadDto.Id)
            //{
            //    return BadRequest("Ids do not match");
            //}
            
            var squadEntity = _mapper.Map<SquadEntity>(squadDto);
            squadEntity.Id = id;
            try
            {
                var squad = await _service.Update(squadEntity);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        // POST: api/Squad
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<SquadEntity>> PostSquadEntity([FromBody] SquadDto squadDto)
        {
            var squadEntity = _mapper.Map<SquadEntity>(squadDto);
            SquadMemberEntity squadMemberEntity = new SquadMemberEntity();

            try
            {
                var squad = await _service.Create(squadEntity);
                squadDto = _mapper.Map<SquadDto>(squad);
                squadMemberEntity.SquadId = squad.Id;
                squadMemberEntity.PlayerId = squad.SquadLeaderId;
                await _sqaudMemberService.Create(squadMemberEntity);

                return CreatedAtAction(nameof(GetSquadEntity), new { id = squadDto.Id }, squadDto);
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }
        

        // DELETE: api/Squad/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SquadEntity>> DeleteSquadEntity(int id)
        {
            try
            {
                var squad = await _service.Delete(id);
                return Ok(squad);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }
    }
}
