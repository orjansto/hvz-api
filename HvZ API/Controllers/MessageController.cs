﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using HvZ_API.Entities;
using HvZ_API.Services.IServices;
using AutoMapper;
using HvZ_API.Dtos;
using HvZ_API.Helpers;
using System.Threading.Tasks;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IAsyncService<MessageEntity> _service;
        private IMapper _mapper;


        public MessageController(IAsyncService<MessageEntity> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/Message
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MessageEntity>>> GetMessageEntities()
        {
            var messages = await _service.GetAll();
            var messagesDto = _mapper.Map<IList<MessageDto>>(messages);
            return Ok(messagesDto);
        }

        // GET: api/Message/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MessageEntity>> GetMessageEntity(int id)
        {
            var message = await _service.GetById(id);

            if (message == null)
            {
                return NotFound();
            }
            var messageDto = _mapper.Map<MessageDto>(message);

            return Ok(messageDto);
        }

        // PUT: api/Message/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMessageEntity(int id, [FromBody] MessageDto messageDto)
        {
            var messageEntity = _mapper.Map<MessageEntity>(messageDto);
            messageEntity.Id = id;
            try
            {
                var message = await _service.Update(messageEntity);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Message
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<MessageEntity>> PostMessageEntity([FromBody] MessageDto messageDto)
        {
            var messageEntity = _mapper.Map<MessageEntity>(messageDto);

            try
            {
                var message = await _service.Create(messageEntity);
                messageDto = _mapper.Map<MessageDto>(message);

                return CreatedAtAction(nameof(GetMessageEntity), new { id = messageDto.Id }, messageDto);
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Message/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MessageEntity>> DeleteMessageEntity(int id)
        {
            try
            {
                var message = await _service.Delete(id);
                return Ok(message);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }


    }
}
