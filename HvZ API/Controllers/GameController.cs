﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using HvZ_API.Entities;
using HvZ_API.Services.IServices;
using HvZ_API.Dtos;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using HvZ_API.Helpers;
using System.Threading.Tasks;

namespace HvZ_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IAsyncService<GameEntity> _service;
        private readonly IAsyncJointService<AdminEntity> _adminService;
        private readonly IAsyncService<PlayerEntity> _playerService;
        private readonly IAsyncService<SquadEntity> _squadService;
        private readonly IAsyncJointService<SquadMemberEntity> _squadMemberService;
        private IMapper _mapper;
        public GameController(
            IAsyncService<GameEntity> service, 
            IAsyncJointService<AdminEntity> adminService,
            IAsyncService<PlayerEntity> playerService,
            IAsyncService<SquadEntity> squadService,
            IAsyncJointService<SquadMemberEntity> squadMemberService,
            IMapper mapper)
        {
            _service = service;
            _adminService = adminService;
            _playerService = playerService;
            _squadService = squadService;
            _squadMemberService = squadMemberService;
            _mapper = mapper;
        }

        // GET: api/Game
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<GameEntity>> GetGameEntities()
        {
            var games = await _service.GetAll();
            var user = this.User.Identity.Name;
            AllGameTypeDto allGameTypeDto = new AllGameTypeDto();
            //var gameDto = _mapper.Map<IList<GameDto>>(games);

            if (user == null)
            {
                foreach (GameEntity game in games)
                {
                    if (game.State.Equals("complete"))
                    {
                        allGameTypeDto.Finished.Add(_mapper.Map<GameDto>(game));
                    } else
                    {
                        allGameTypeDto.Games.Add(_mapper.Map<GameDto>(game));
                    }
                }
            } else
            {
                var playerEntities = await _playerService.GetAll();
                var userId = int.Parse(user);

                foreach(PlayerEntity player in playerEntities)
                {
                    if(userId == player.UserId)
                    {
                        foreach(GameEntity game in games)
                        {
                            if(player.GameId == game.Id)
                            {
                                IEnumerable<PlayerEntity> players = await _playerService.GetAllById(game.Id);
                                int i = 0;
                                foreach (PlayerEntity entity in players)
                                {
                                    i++;
                                }

                                var gameDto = _mapper.Map<GameDto>(game);
                                gameDto.NumberOfPlayers = i;
                                if (!game.State.Equals("complete"))
                                {
                                    allGameTypeDto.Registered.Add(gameDto);
                                }
                                else if (game.State.Equals("complete"))
                                {
                                    allGameTypeDto.Finished.Add(gameDto);
                                }
                                else
                                {
                                    allGameTypeDto.Games.Add(gameDto);
                                }
                            } 
                        }
                    }
                }
            }            

            return Ok(allGameTypeDto);
        }

        // GET: api/Game/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GameEntity>> GetGameEntity(int id)
        {
            var game = await _service.GetById(id);

            if (game == null)
            {
                return NotFound();
            }
            var userId = int.Parse(this.User.Identity.Name);
            var gameDto = _mapper.Map<GameDto>(game);
            gameDto.registered = false;

            IEnumerable<PlayerEntity> players = await _playerService.GetAllById(game.Id);
            int i = 0;
            foreach (PlayerEntity entity in players)
            {
                if(entity.Id == userId)
                {
                    gameDto.registered = true;
                }
                i++;
            }
            
            gameDto.NumberOfPlayers = i;

            var squads = await _squadService.GetAll();

            foreach(SquadEntity squad in squads)
            {
                if(squad.GameId == game.Id)
                {
                    var squadDto = _mapper.Map<SquadDto>(squad);
                    var squadMemberlist = await _squadMemberService.GetAllEntitiesBySquadId(squad.Id);
                    foreach(SquadMemberEntity squadMember in squadMemberlist)
                    {
                        var squadPlayer = _playerService.GetById(squadMember.PlayerId);
                        squadDto.SquadMember.Add(_mapper.Map<PlayerDto>(squadPlayer));
                    }
                    gameDto.squads.Add(squadDto);
                }
            }
            
            return Ok(gameDto);
        }

        // PUT: api/Game/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGameEntity(int id, [FromBody] GameDto gameDto)
        {
            var gameEntity = _mapper.Map<GameEntity>(gameDto);
            gameEntity.Id = id;
            try
            {
                var game = await _service.Update(gameEntity);
                return Ok();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        // POST: api/Game
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<GameEntity>> PostGameEntity([FromBody] GameDto gameDto)
        {
            var gameEntity = _mapper.Map<GameEntity>(gameDto);
            AdminEntity adminEntity = new AdminEntity();

            try
            {
                var game = await _service.Create(gameEntity);
                gameDto = _mapper.Map<GameDto>(game);
                adminEntity.GameId = game.Id;
                adminEntity.UserId = int.Parse(this.User.Identity.Name);
                await _adminService.Create(adminEntity);

                return CreatedAtAction(nameof(GetGameEntity), new { id = gameDto.Id }, gameDto);
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }

        // DELETE: api/Game/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<GameEntity>> DeleteGameEntity(int id)
        {
            try
            {
                var gaem = await _service.Delete(id);
                return Ok(gaem);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }
    }
}
