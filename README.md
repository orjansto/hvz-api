## Humans versus Zombies

### Description

This is the official repository for the Humans versus Zombies website and game
engine.
The reference implementation showcasing all the features is live at 
https://hvzapi.azurewebsites.net

###  API Document

#### POST/Register URL
```/User/register```

Registers the user in the authentication database.

#### Accepts parameters:

- email :: Unique user identifier.
- password :: Plain text password for later authentication.

Responds with 403 Forbidden if the user already exists. Responds 
with 201 Created if the user is successfully registere

#### POST/Login URL
```/authenticate```

Authenticates a previously registered user.

#### Accepts parameters
- email :: Unique user identifier.
- password :: Plain text password for later authentication.

Responds with 401 Unauthorized if the authentication failed. Responds with 200 OK
if the user is successfully authenticated.


### Technologies 

- ASP.NET Core 3.0 
- SQL Server
- React

### Maintainers

- Huy
- Ørjan
- Azadeh




